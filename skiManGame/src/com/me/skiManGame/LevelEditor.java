package com.me.skiManGame;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class LevelEditor implements Screen {
	final skiManGame game;
	TextureAtlas placeHolderAtlas;
	OrthographicCamera camera;
	Stage stage;
	InputMultiplexer inputMux;
	LevelCanvas levelCanvas;
	ArrayList<String> levelNames;
	SelectBox levelNamesBox;
	SelectBox chooseTool;
	Label propertiesTitle;
	PropertyInfo selectedPolyInfo;
	Table uiTable;
	HashMap<String, Tool> editingTools;
	String[] editingToolNames;
	
	public LevelEditor(final skiManGame game){
		this.game = game;

		camera = new OrthographicCamera();
		camera.setToOrtho(false);
		
		levelCanvas = new LevelCanvas(camera);
		
		placeHolderAtlas = new TextureAtlas(Gdx.files.internal("placeHolderGraphics.atlas"));
		
		editingToolNames = new String[]{"Rectangle", "Triangle", "Spike", "Detector", "Translate", "Rotate", "Scale", "Set Start", "Set Goal", "Delete"};
		editingTools = new HashMap<String, Tool>();
		editingTools.put("Rectangle", Tool.CREATE_RECTANGLE);
		editingTools.put("Triangle", Tool.CREATE_TRIANGLE);
		editingTools.put("Spike", Tool.CREATE_SPIKE);
		editingTools.put("Detector", Tool.CREATE_DETECTOR);
		editingTools.put("Translate", Tool.TRANSLATE);
		editingTools.put("Rotate", Tool.ROTATE);
		editingTools.put("Scale", Tool.SCALE);
		editingTools.put("Set Start", Tool.SET_START);
		editingTools.put("Set Goal", Tool.SET_GOAL);
		editingTools.put("Delete", Tool.DELETE);
		
		stage = new Stage();

		TextButton saveButton = new TextButton("Save", game.skin);
		saveButton.addListener(new ClickListener(){
			public void clicked(InputEvent event, float x, float y){
				levelCanvas.saveLevel(levelNamesBox.getSelection());
			}
		});
		
		levelNames = new ArrayList<String>();
		for(FileHandle levelFile : Gdx.files.local("levels").list()){
			levelNames.add(levelFile.name());
		}
		levelNamesBox = new SelectBox(levelNames.toArray(), game.skin);
		
		if(levelNames.size() >=1){
			levelNamesBox.setSelection(0);
			levelCanvas.loadLevel(levelNamesBox.getSelection());
		}
		
		TextButton loadButton = new TextButton("Load", game.skin);
		loadButton.addListener(new ClickListener(){
			public void clicked(InputEvent event, float x, float y){
				levelCanvas.loadLevel(levelNamesBox.getSelection());
			}
		});
		
		TextButton newButton = new TextButton("New", game.skin);
		newButton.addListener(new ClickListener(){
			public void clicked(InputEvent event, float x, float y){
				levelCanvas.clear();
				int highestLevel = 0;
				for(FileHandle levelFile : Gdx.files.local("levels").list()){
                    int levelNumber = Integer.parseInt(levelFile.nameWithoutExtension().split("level")[1]);
                    if(levelNumber > highestLevel){
                            highestLevel = levelNumber;
                    }
				}
				int newLevelNum = highestLevel + 1;
				String newLevelName = "level" + newLevelNum + ".json";
				levelCanvas.clear();
				levelCanvas.saveLevel(newLevelName);
				//Update the list box selection
				levelNames.add(newLevelName);
				levelNamesBox.setItems(levelNames.toArray());
				levelNamesBox.setSelection(levelNames.size() - 1);
			}
		});
		
		TextButton clearButton = new TextButton("Clear", game.skin);
		clearButton.addListener(new ClickListener(){
			public void clicked(InputEvent event, float x, float y){
				levelCanvas.clear();
			}
		});

		
		TextButton testButton = new TextButton("Test Level", game.skin);
		testButton.addListener(new ClickListener(){
			public void clicked(InputEvent event, float x, float y){
				dispose();
				String levelName = levelNamesBox.getSelection();
				int levelNum = Integer.parseInt(levelName.substring(5, levelName.lastIndexOf('.')));
				game.setScreen(new GameScreen(game, levelNum));
			}
		});
		

		chooseTool = new SelectBox(editingToolNames, game.skin);
		chooseTool.addListener(new ChangeListener(){
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				levelCanvas.currentTool = editingTools.get(chooseTool.getSelection());
			}
		});
		
		propertiesTitle = new Label("Selected Polygon Properties", game.skin);
		selectedPolyInfo = new PropertyInfo(game.skin);
		
		Table stageTable = new Table();
		stageTable.setFillParent(true);
		stageTable.right();
		
		uiTable = new Table();
		uiTable.setTouchable(Touchable.enabled);
		uiTable.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y){
			}
		});
		uiTable.setBackground(new TextureRegionDrawable(placeHolderAtlas.findRegion("exampleBlock0")));
		uiTable.debug();
		uiTable.pad(10);
		uiTable.row();
		uiTable.add(chooseTool);
		uiTable.row();
		uiTable.add(levelNamesBox);
		uiTable.row();
		uiTable.add(saveButton);
		uiTable.row();
		uiTable.add(loadButton);
		uiTable.row();
		uiTable.add(clearButton);
		uiTable.row();
		uiTable.add(newButton);
		uiTable.row();
		uiTable.add(testButton);
		uiTable.row();
		uiTable.add(propertiesTitle);
		uiTable.row();
		selectedPolyInfo.addElementsToTable(uiTable);
		
		stageTable.add(uiTable).height(stage.getHeight());
		stage.addActor(stageTable);
		
		inputMux = new InputMultiplexer(stage);
		inputMux.addProcessor(levelCanvas);
		Gdx.input.setInputProcessor(inputMux);
	}
	@Override
	public void render(float delta) {		
		//Update Part
		camera.update();
		game.batch.setProjectionMatrix(camera.combined);
		game.shapeRenderer.setProjectionMatrix(camera.combined);
		moveCamera();
		
		//Rendering Part
		Gdx.gl.glClearColor(1,1,1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		Polygon selectedPolygon = levelCanvas.getSelectedShape();
		if(selectedPolygon != null){
			selectedPolyInfo.update(selectedPolygon);
		}
		levelCanvas.render(game);
		
		game.batch.begin();
		game.batch.end();
		
		stage.act();
		stage.draw();
		
		checkKeyboardShortcuts();

//		Table.drawDebug(stage);

	}
	
	public void checkKeyboardShortcuts(){
		if(Gdx.input.isKeyPressed(Keys.T)){
			chooseTool.setSelection("Translate");
			levelCanvas.currentTool = editingTools.get(chooseTool.getSelection());
		}
		else if(Gdx.input.isKeyPressed(Keys.S)){
			chooseTool.setSelection("Scale");
			levelCanvas.currentTool = editingTools.get(chooseTool.getSelection());
		}
		else if(Gdx.input.isKeyPressed(Keys.R)){
			chooseTool.setSelection("Rotate");
			levelCanvas.currentTool = editingTools.get(chooseTool.getSelection());
		}
		else if(Gdx.input.isKeyPressed(Keys.G)){
			chooseTool.setSelection("Set Goal");
			levelCanvas.currentTool = editingTools.get(chooseTool.getSelection());
		}
		else if(Gdx.input.isKeyPressed(Keys.D)){
			chooseTool.setSelection("Delete");
			levelCanvas.currentTool = editingTools.get(chooseTool.getSelection());
		}
		else if(Gdx.input.isKeyPressed(Keys.Y)){
			chooseTool.setSelection("Triangle");
			levelCanvas.currentTool = editingTools.get(chooseTool.getSelection());
		}
		else if(Gdx.input.isKeyPressed(Keys.B)){
			chooseTool.setSelection("Rectangle");
			levelCanvas.currentTool = editingTools.get(chooseTool.getSelection());
		}
		else if(Gdx.input.isKeyPressed(Keys.P)){
			chooseTool.setSelection("Set Start");
			levelCanvas.currentTool = editingTools.get(chooseTool.getSelection());
		}
		else if(Gdx.input.isKeyPressed(Keys.H)){
			chooseTool.setSelection("Spike");
			levelCanvas.currentTool = editingTools.get(chooseTool.getSelection());
		}
		else if(Gdx.input.isKeyPressed(Keys.M)){
			chooseTool.setSelection("Detector");
			levelCanvas.currentTool = editingTools.get(chooseTool.getSelection());
		}

	}
	public void moveCamera(){
		if(Gdx.input.isKeyPressed(Keys.UP)){
			camera.translate(0, 30);
		}
		if(Gdx.input.isKeyPressed(Keys.DOWN)){
			camera.translate(0, -30);
		}
		if(Gdx.input.isKeyPressed(Keys.LEFT)){
			camera.translate(-30,  0);
		}
		if(Gdx.input.isKeyPressed(Keys.RIGHT)){
			camera.translate(30, 0);
		}
		
		if(camera.position.x < camera.viewportWidth / 2){
			camera.position.x = camera.viewportWidth / 2;
		}
		if(camera.position.y < camera.viewportHeight / 2){
			camera.position.y = camera.viewportHeight / 2;
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		placeHolderAtlas.dispose();
	}


}
