package com.me.skiManGame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Goal {
	Rectangle collisionArea;
	TextureRegion goalImage;
	
	public Goal(Vector2 position, TextureAtlas atlas){
		collisionArea = new Rectangle(position.x, position.y, 50, 40);
		goalImage = atlas.findRegion("goal");
	}
	
	public void render(SpriteBatch batch){
		batch.draw(goalImage, collisionArea.x, collisionArea.y, 50, 40);
	}
	
	public Vector2 getPosition(){
		return new Vector2(collisionArea.x, collisionArea.y);
	}
	
	public void setPosition(float x, float y){
		collisionArea.setPosition(x, y);
	}
	
	
}
