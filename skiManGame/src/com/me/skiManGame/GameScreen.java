package com.me.skiManGame;

import java.util.ArrayList;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.ShortArray;

public class GameScreen implements Screen {
	final skiManGame game;
	TextureAtlas placeHolderAtlas;
	OrthographicCamera camera;
	Player player;
	
	ArrayList<StaticCollisionPolygon> obstacles;
	ArrayList<Spike> spikes;
	ArrayList<Detector> detectors;
	CollisionInfo collisionResult;
	Goal goal;
	LevelData levelData;
	int levelNum;
	
	Sound spikeSound;
	Sound detectorSound;
	Sound goalSound;
	Sound fallSound;
	Music snowMusic;
	
	public GameScreen(final skiManGame game, int lNum){
		this.game = game;
		levelNum = lNum;

		camera = new OrthographicCamera();
		camera.setToOrtho(false);
		placeHolderAtlas = new TextureAtlas(Gdx.files.internal("placeHolderGraphics.atlas"));
		player = new Player(placeHolderAtlas, 0, 200);
		collisionResult = new CollisionInfo(CollisionType.NONE, null);
		spikeSound = Gdx.audio.newSound(Gdx.files.internal("sounds/spike.wav"));
		detectorSound = Gdx.audio.newSound(Gdx.files.internal("sounds/detector.wav"));
		goalSound = Gdx.audio.newSound(Gdx.files.internal("sounds/Powerup.wav"));
		fallSound = Gdx.audio.newSound(Gdx.files.internal("sounds/fall.wav"));
		
		loadLevel("level" + levelNum + ".json");
		
		Gdx.input.setInputProcessor(player);
	}
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(158/255.0f,218/255.0f,255/255.0f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		camera.update();
		game.batch.setProjectionMatrix(camera.combined);
		game.shapeRenderer.setProjectionMatrix(camera.combined);
		
		game.batch.begin();
		
		Vector3 textCoords = new Vector3(10, 10, 0);
		camera.unproject(textCoords);

		game.font.draw(game.batch, "Level " + levelNum, textCoords.x, textCoords.y);
		player.render(game.batch);
		for(StaticCollisionPolygon obstacle : obstacles){
			obstacle.render(game);
		}
		
		for(Spike spike : spikes){
			spike.render(game.batch);
		}
		
		for(Detector detector : detectors){
			detector.render(game.batch);
		}
		
		goal.render(game.batch);
		game.batch.end();
		
		player.update();
		
		//Dont let him go too far left or right
		if(player.getX() < 0){
			player.setX(0);
		}
		
		
		collisionResult.collisionType = CollisionType.NONE;
		//Normal Collision System
		if(player.playerState == PlayerState.NORMAL){
			for(StaticCollisionPolygon obstacle : obstacles){
				collisionResult = Collider.playerRectCollision(player.collisionSurface, obstacle.polygon);
				if(collisionResult.collisionType == CollisionType.GROUND){
					if(player.onGroundTimer <= 0 && !Gdx.input.isKeyPressed(Keys.LEFT) && !Gdx.input.isKeyPressed(Keys.RIGHT)){
						//Player shouldn't slide when hitting the ground from the air;
						player.vx = 0;
					}
					player.onGroundTimer = player.groundSlackTime;
					player.vy = 0;

				}
				else if(collisionResult.collisionType == CollisionType.NORMAL){
					player.jumping = false;
					Vector2 velocityVector = new Vector2(player.vx, player.vy);
					player.vx = velocityVector.x - 2 * ((velocityVector.dot(collisionResult.axisNormal))) * collisionResult.axisNormal.x * 0.5f;
					player.vy = velocityVector.y - 2 * ((velocityVector.dot(collisionResult.axisNormal))) * collisionResult.axisNormal.y * 0.8f;
				}
			}
		}
		//Ski Collision System
		else{
			for(StaticCollisionPolygon obstacle : obstacles){
				collisionResult = Collider.playerRectCollisionSAT(player.collisionSurface, obstacle.polygon);
				if(collisionResult.collisionType == CollisionType.GROUND){
					player.onGroundTimer = player.groundSlackTime;
				}
				if(collisionResult.collisionType != CollisionType.NONE){
					player.jumping = false;
					Vector2 velocityVector = new Vector2(player.vx, player.vy);
					if(collisionResult.axisNormal.y == 0){
						player.vx = Math.abs(velocityVector.x) * (collisionResult.axisNormal.x / Math.abs(collisionResult.axisNormal.x));
					}
					else{
						player.vx = velocityVector.x - 2 * ((velocityVector.dot(collisionResult.axisNormal))) * collisionResult.axisNormal.x * 0.9f;
						player.vy = velocityVector.y - 2 * ((velocityVector.dot(collisionResult.axisNormal))) * collisionResult.axisNormal.y * 0.9f;
					}
				}
			}
		}

		Vector3 playerCamPosition = new Vector3(player.getX(), player.getY(), 0);
		camera.project(playerCamPosition);

		float rightBoundary = (camera.viewportWidth / 2) + 100;
		float leftBoundary = (camera.viewportWidth / 2) - 100;
		float upperBoundary = (camera.viewportHeight / 2) + 100;
		float lowerBoundary = (camera.viewportHeight / 2) - 100;
		
		if(playerCamPosition.x < leftBoundary){
			camera.position.x -= leftBoundary- playerCamPosition.x;
		}
		if(playerCamPosition.x > rightBoundary){
			camera.position.x += playerCamPosition.x - rightBoundary;
		}
		if(camera.position.x < camera.viewportWidth / 2){
			camera.position.x = camera.viewportWidth / 2;
		}
		
		if(playerCamPosition.y < lowerBoundary){
			camera.position.y -= lowerBoundary - playerCamPosition.y;
		}
		if(playerCamPosition.y > upperBoundary){
			camera.position.y += playerCamPosition.y - upperBoundary;
		}
		if(camera.position.y < camera.viewportHeight / 2){
			camera.position.y = camera.viewportHeight / 2;
		}

		checkForDeath();
		checkForWin();
	}
	
	public void checkForDeath(){
		if(player.getY() < 0){
			player.die();
			player.setPosition(levelData.playerStart);
			fallSound.play();
		}
		for(Spike spike : spikes){
			if(spike.collisionArea.overlaps(player.collisionSurface)){
				player.die();
				spikeSound.play();
				player.setPosition(levelData.playerStart);
			}
		}
		for(Detector detector : detectors){
			if(detector.collisionArea.overlaps(player.collisionSurface) && !player.isFrozen()){
				player.die();
				detectorSound.play();
				player.setPosition(levelData.playerStart);
			}
		}
	}
	
	public void checkForWin(){
		if(goal.collisionArea.overlaps(player.collisionSurface)){
			goalSound.play();
			if(Gdx.files.local("levels/level" + (levelNum + 1) + ".json").exists()){
				game.setScreen(new GameScreen(game, levelNum + 1));
			}
			else{
				game.setScreen(new WinScreen(game));
			}

		}
	}
	
	public void loadLevel(String levelName){
		Json json = new Json();
		levelData = new LevelData();
		levelData = json.fromJson(levelData.getClass(), Gdx.files.local("levels/" + levelName));
		
		obstacles = new ArrayList<StaticCollisionPolygon>();
		for(Polygon polygon : levelData.polygons){
				obstacles.add(new StaticCollisionPolygon(polygon.getTransformedVertices()));
		}
		spikes = new ArrayList<Spike>();
		for(SpikeInfo spikeInfo : levelData.spikesInfo){
			spikes.add(new Spike(placeHolderAtlas, spikeInfo.location.x, spikeInfo.location.y, spikeInfo.rotation));
		}
		detectors = new ArrayList<Detector>();
		for(DetectorInfo detectorInfo : levelData.detectorsInfo){
			detectors.add(new Detector(detectorInfo.position, detectorInfo.dimensions, placeHolderAtlas));
		}
		player.setPosition(levelData.playerStart);
		goal = new Goal(levelData.goal, placeHolderAtlas);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		placeHolderAtlas.dispose();
	}

}
