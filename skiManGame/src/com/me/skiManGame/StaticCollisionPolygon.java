package com.me.skiManGame;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class StaticCollisionPolygon {

	float[] points;
	Polygon polygon;
	
	public StaticCollisionPolygon(float[] polyPoints){
		points = polyPoints;
		polygon = new Polygon(points);
	}
	
	public void update(){
		
	}
	
	public void render(skiManGame game){
		game.batch.end();
		game.shapeRenderer.begin(ShapeType.Filled);
		game.shapeRenderer.setColor(Color.WHITE);
		if(points.length / 2 == 3){
			game.shapeRenderer.triangle(points[0], points[1], points[2], points[3], points[4], points[5]);
		}
		else if(points.length / 2 == 4){
			game.shapeRenderer.rect(points[0], points[1], points[4] - points[0], points[5] - points[1]);
		}
		game.shapeRenderer.end();
		game.batch.begin();
		
	}
}
