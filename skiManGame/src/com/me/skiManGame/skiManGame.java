package com.me.skiManGame;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class skiManGame extends Game {
	public SpriteBatch batch;
	public Skin skin;
	public ShapeRenderer shapeRenderer;
	public BitmapFont font;
	Music snowMusic;
	
	@Override
	public void create() {		
		batch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();
		font = new BitmapFont();
		skin = new Skin(Gdx.files.internal("uiskin.json"));
		snowMusic = Gdx.audio.newMusic(Gdx.files.internal("sounds/snowMusic.mp3"));
		snowMusic.setLooping(true);
		snowMusic.setVolume(0.25f);
//		this.setScreen(new MenuScreen(this));
		this.setScreen(new LevelEditor(this));
//		this.setScreen(new WinScreen(this));
		

	}

	@Override
	public void render() {		
		super.render();
	}
	
	@Override
	public void dispose(){
		batch.dispose();
		font.dispose();
		skin.dispose();
		snowMusic.dispose();
		shapeRenderer.dispose();
	}

}
