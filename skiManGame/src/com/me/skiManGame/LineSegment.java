package com.me.skiManGame;

import com.badlogic.gdx.math.Vector2;

public class LineSegment {
	float startX;
	float startY;
	float endX;
	float endY;
	
	public LineSegment(float stX, float stY, float eX, float eY){
		startX = stX;
		startY = stY;
		endX = eX;
		endY = eY;
	}
	
	public Vector2 getVector(){
		return new Vector2(endX - startX, endY - startY);
	}
	
	public Vector2 getNormalVector(){
		return new Vector2(endX - startX, endY - startY).nor();
	}
	
	public Vector2 getCentre(){
		return new Vector2( (startX + endX) / 2, (startY + endY) / 2);
	}
}
