package com.me.skiManGame;

import com.badlogic.gdx.math.Vector2;

public class CollisionInfo {
	CollisionType collisionType;
	Vector2 axisNormal;
	public CollisionInfo(CollisionType colType, Vector2 axNorm){
		collisionType = colType;
		axisNormal = axNorm;
	}
}
