package com.me.skiManGame;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class PropertyInfo {
	Label xLabel;
	Label yLabel;
	Label rotationLabel;
	Label scaleLabelX;
	Label scaleLabelY;
	PropertyInfo(Polygon poly, Skin skin){
		xLabel = new Label("X : " + poly.getX(), skin);
		yLabel = new Label("Y : " + poly.getY(), skin);
		rotationLabel = new Label("Rotation: " + poly.getRotation(), skin);
		scaleLabelX = new Label("Scale X: " + poly.getScaleX(), skin);
		scaleLabelY = new Label("Scale Y: " + poly.getScaleY(), skin);
		
	}
	
	PropertyInfo(Skin skin){
		xLabel = new Label("X: ", skin);
		yLabel = new Label("Y: ", skin);
		rotationLabel = new Label("Rotation: ", skin);
		scaleLabelX = new Label("Scale X: " , skin);
		scaleLabelY = new Label("Scale Y: " , skin);
	}
	
	public void update(Polygon poly){
		xLabel.setText("X : " + poly.getX());
		yLabel.setText("Y : " + poly.getY());
		rotationLabel.setText("Rotation: " + poly.getRotation());
		scaleLabelX.setText("Scale X: " + poly.getScaleX());
		scaleLabelY.setText("Scale Y: " + poly.getScaleY());
	}
	
	public void addElementsToTable(Table table){
		table.add(xLabel);
		table.row();
		table.add(yLabel);
		table.row();
		table.add(rotationLabel);
		table.row();
		table.add(scaleLabelX);
		table.row();
		table.add(scaleLabelY);
		table.row();
	}
}
