package com.me.skiManGame;

import com.badlogic.gdx.math.Vector2;

public class SpikeInfo {
	Vector2 location;
	float rotation;
	
	public SpikeInfo(){
		location = new Vector2();
		rotation = 0;
	}
	
	public SpikeInfo(Vector2 loc, float rot){
		location = loc;
		rotation = rot;
	}
}
