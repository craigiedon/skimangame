package com.me.skiManGame;

import com.badlogic.gdx.math.Vector2;

public class DetectorInfo {
	Vector2 position;
	Vector2 dimensions;
	
	public DetectorInfo(){
		position = new Vector2();
		dimensions = new Vector2();
	}
	
	public DetectorInfo(Vector2 position, Vector2 dimensions){
		this.position = position;
		this.dimensions = dimensions;
	}

	
}
