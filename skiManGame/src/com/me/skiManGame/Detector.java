package com.me.skiManGame;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Detector {
	Rectangle collisionArea;
	TextureRegion detectorImage;
	
	public Detector(float x, float y, TextureAtlas atlas) {
		collisionArea = new Rectangle(x,y,0, 0);
		detectorImage = atlas.findRegion("exampleBlock5");
	}
	
	public Detector(Vector2 position, Vector2 dimensions, TextureAtlas atlas){
		collisionArea = new Rectangle(position.x,position.y, dimensions.x, dimensions.y);
		detectorImage = atlas.findRegion("exampleBlock5");
	}

	public void setPosition(float x, float y){
		collisionArea.setPosition(x, y);
	}
	
	public Vector2 getPosition(){
		return new Vector2(collisionArea.x, collisionArea.y);
	}
	
	public Vector2 getDimensions(){
		return new Vector2(collisionArea.width, collisionArea.height);
	}
	public void setDimensions(float w, float h){
		collisionArea.width = w;
		collisionArea.height = h;
	}
	
	public void render(SpriteBatch batch){
		batch.setColor(1, 1, 1, 0.2f);
		batch.draw(detectorImage, collisionArea.x, collisionArea.y, collisionArea.width, collisionArea.height);
		batch.setColor(Color.WHITE);
	}
}
