package com.me.skiManGame;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;

public class StaticCollisionRectangle extends Rectangle {
	
	public StaticCollisionRectangle(int x, int y, int width, int height){
		super(x, y, width, height);
	}
	
	public void render(skiManGame game){
		game.batch.end();
		
		game.shapeRenderer.begin(ShapeType.Filled);
		game.shapeRenderer.setColor(Color.RED);
		game.shapeRenderer.rect(x, y, width, height);
		game.shapeRenderer.end();
		
		game.batch.begin();
	}
	
	public void update(){
		
	}
}
