package com.me.skiManGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class MenuScreen implements Screen {

	final skiManGame game;
	OrthographicCamera camera;
	Stage stage;
	public MenuScreen(final skiManGame game){
		this.game = game;
		camera = new OrthographicCamera();
		camera.setToOrtho(false);
		
		stage = new Stage();
		Table uiTable = new Table();
		uiTable.setFillParent(true);
		
		Label dummyLabel = new Label("Polar Freeze", game.skin);
		
		TextButton playButton = new TextButton("Play", game.skin);
		playButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y){
				game.snowMusic.play();
				game.setScreen(new GameScreen(game, 1));
			}
		});
		
		uiTable.add(dummyLabel);
		uiTable.row();
		uiTable.add(playButton);
								
		stage.addActor(uiTable);
		Gdx.input.setInputProcessor(stage);
	}
	@Override
	public void render(float delta) {
		camera.update();
		game.batch.setProjectionMatrix(camera.combined);
		game.shapeRenderer.setProjectionMatrix(camera.combined);
		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		stage.act();
		stage.draw();

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
