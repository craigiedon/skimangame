package com.me.skiManGame;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class Player extends InputAdapter{
	float vx = 0;
	float vy = 0;
	float ax = 0;
	float ay = 0;
	public static final float normalFrictionX = 0.8f;
	public static final float skiFrictionX = 0.99f;
	public static final float airFrictionX = 0.5f;
	float frictionX = 0.8f;
	float frictionY = 0.9f;
	float gravity = 50;
	float minVy = -500;
	float maxVx = 400;
	float timeSinceLastJump = 0;
	
	public static final float groundSlackTime = 0.25f;
	float onGroundTimer = groundSlackTime;
	boolean jumping = false;
	public static final float jumpImpulse = 500;
	public static final float stopJumpValue = 200;
	public static final float moveSpeed = 30;
	
	PlayerState playerState = PlayerState.NORMAL;
	
	Rectangle collisionSurface;
	TextureRegion idleImageLeft;
	TextureRegion idleImageRight;
	TextureRegion frozenImageLeft;
	TextureRegion frozenImageRight;
	TextureRegion jumpImageLeft;
	TextureRegion jumpImageRight;
	Direction facing;
	
	float animTime = 0;
	Animation runRight;
	Animation runLeft;
	
	Sound jumpSound;
	Sound freezeSound;
	
	public Player(TextureAtlas atlas, int x, int y){
		idleImageLeft = atlas.findRegion("idleStateLeft");
		idleImageRight = atlas.findRegion("idleStateRight");
		frozenImageLeft = atlas.findRegion("frozenLeft");
		frozenImageRight = atlas.findRegion("frozenRight");
		jumpImageLeft = atlas.findRegion("jumpingLeft");
		jumpImageRight = atlas.findRegion("jumpingRight");
		jumpSound = Gdx.audio.newSound(Gdx.files.internal("sounds/jump.wav"));
		freezeSound = Gdx.audio.newSound(Gdx.files.internal("sounds/freeze.wav"));
		
		Array<TextureRegion> rightWalkFrames = new Array<TextureRegion>();
		Array<TextureRegion> leftWalkFrames = new Array<TextureRegion>();
		for(int i = 1; i <= 12; i++){
			if(i >= 10){
				leftWalkFrames.add(atlas.findRegion("polarbearLeft00" + Integer.toString(i)));
				rightWalkFrames.add(atlas.findRegion("polarbearRight00" + Integer.toString(i)));
			}
			else{
				leftWalkFrames.add(atlas.findRegion("polarbearLeft000" + Integer.toString(i)));
				rightWalkFrames.add(atlas.findRegion("polarbearRight000" + Integer.toString(i)));
			}
		}
		
		runRight = new Animation(0.03f, rightWalkFrames, Animation.LOOP);
		runLeft = new Animation(0.03f, leftWalkFrames, Animation.LOOP);
		
		collisionSurface = new Rectangle(x, y, 19, 28);
		facing = Direction.RIGHT;
	}
	
	@Override
	public boolean keyDown(int keycode){
		if(playerState == PlayerState.NORMAL){
			switch(keycode){
			case Keys.SPACE:
				if(onGroundTimer > 0){
					jumpSound.play(0.3f);
					jumping = true;
					onGroundTimer = 0;
					timeSinceLastJump = 0;
				}
				break;
			case Keys.DOWN:
				break;
			case Keys.LEFT:
				facing = Direction.LEFT;
				ax = -moveSpeed;
				vx = ax;
				frictionX = 1;;
				break;
			case Keys.RIGHT:
				facing = Direction.RIGHT;
				ax = moveSpeed;
				vx = ax;
				frictionX = 1;
				animTime = 0;
				break;
			case Keys.SHIFT_LEFT:
				freezeSound.play();
				playerState = PlayerState.SKI;
				ax = 0;
				frictionX = skiFrictionX;
			}
		}
		
		return true;
	}
	
	@Override
	public boolean keyUp(int keycode){
		if(playerState == PlayerState.NORMAL){
			switch(keycode){
			case Keys.SPACE:
				jumping = false;
				if(vy > stopJumpValue){
					vy = stopJumpValue;
				}
				break;
			case Keys.DOWN:
				break;
			case Keys.LEFT:
				if(Gdx.input.isKeyPressed(Keys.RIGHT)){
					facing = Direction.RIGHT;
					ax = moveSpeed;
					vx = ax;
					frictionX = 1;
				}
				else{
					if(onGroundTimer <= 0){
						ax = 0;
						frictionX = airFrictionX;
					}
					else{
						vx /= 7;
						ax = 0;
						frictionX = normalFrictionX;
					}
				}
				break;
			case Keys.RIGHT:
				if(Gdx.input.isKeyPressed(Keys.LEFT)){
					facing = Direction.LEFT;
					ax = -moveSpeed;
					vx = ax;
					frictionX = 1;
				}
				else{
					if(onGroundTimer <=  0){
						ax = 0;
						frictionX = airFrictionX;
					}
					else{
						vx /= 7;
						ax = 0;
						frictionX = normalFrictionX;
					}
				}
				break;
			}
		}
		else{
			switch(keycode){
			case Keys.SHIFT_LEFT:
				playerState = PlayerState.NORMAL;
				frictionX = normalFrictionX;
				if(Gdx.input.isKeyPressed(Keys.RIGHT)){
					ax = moveSpeed;
					if(vx < 0){
						vx = moveSpeed;
					}
					frictionX = 1;
				}
				if(Gdx.input.isKeyPressed(Keys.LEFT)){
					ax = -moveSpeed;
					if(vx > 0){
						vx = moveSpeed;
					}
					frictionX = 1;
				}
			}
		}

		return true;
	}
	
	public void update(){
		

		vx += ax;
		vy += ay - gravity;
		
		if(jumping){
			timeSinceLastJump += Gdx.graphics.getDeltaTime();
			vy = jumpImpulse;
			if(timeSinceLastJump > 0.2){
				jumping = false;
				if(vy > stopJumpValue){
					vy = stopJumpValue;
				}
			}
		}
		
		vx *= frictionX;
		
		if(vx > maxVx){
			vx = maxVx;
		}
		else if(vx < -maxVx){
			vx = -maxVx;
		}
		if(vy < minVy){
			vy = minVy;
		}
		
		if(Math.abs(vx) < 2){
			vx = 0;
		}
		if(Math.abs(vy) < 2){
			vy = 0;
		}
		
		if(Math.abs(vx) > 0){
			animTime += Gdx.graphics.getDeltaTime();
		}

		collisionSurface.setX(collisionSurface.getX() + Gdx.graphics.getDeltaTime() * vx);
		collisionSurface.setY(collisionSurface.getY() + Gdx.graphics.getDeltaTime() * vy);
		
		onGroundTimer -= Gdx.graphics.getDeltaTime();
	}
	
	public Vector2 getPosition(){
		return new Vector2(collisionSurface.getX(), collisionSurface.getY());
	}
	
	public void setPosition(Vector2 pos){
		collisionSurface.x = pos.x;
		collisionSurface.y = pos.y;
	}
	
	public void setX(float x){
		collisionSurface.x = x;	
	}
	
	public void setY(float y){
		collisionSurface.y = y;
	}
	
	public void translate(float x, float y){
		collisionSurface.x += x;
		collisionSurface.y += y;
	}
	
	public float getX(){
		return collisionSurface.x;
	}
	
	public float getY(){
		return collisionSurface.y;
	}
	
	
	public void render(SpriteBatch batch){
		if(playerState == PlayerState.NORMAL){
			if(onGroundTimer <= 0){
				if(facing == Direction.LEFT){
					batch.draw(jumpImageLeft, collisionSurface.getX(), collisionSurface.getY());
				}
				else{
					batch.draw(jumpImageRight,  collisionSurface.getX(), collisionSurface.getY());
				}
			}
			else{
				if(!Gdx.input.isKeyPressed(Keys.LEFT) && !Gdx.input.isKeyPressed(Keys.RIGHT)){
					if(facing == Direction.LEFT){
						batch.draw(idleImageLeft, collisionSurface.getX(), collisionSurface.getY());
					}
					else{
						batch.draw(idleImageRight, collisionSurface.getX(), collisionSurface.getY());
					}

				}
				else{
					if(facing == Direction.RIGHT){
						batch.draw(runRight.getKeyFrame(animTime), collisionSurface.getX() - 3, collisionSurface.getY() - 1);
					}
					else{
						batch.draw(runLeft.getKeyFrame(animTime), collisionSurface.getX() - 3, collisionSurface.getY() - 1);
					}
				}
			}
		}
		else{
			if(facing == Direction.LEFT){
				batch.draw(frozenImageLeft, collisionSurface.getX(), collisionSurface.getY());
			}
			else{
				batch.draw(frozenImageRight, collisionSurface.getX(), collisionSurface.getY());
			}
		}
		
		
	}
	
	public void die(){
		vx = 0;
		vy = 0;
	}
	
	public boolean isFrozen(){
		return playerState == PlayerState.SKI;
	}
}