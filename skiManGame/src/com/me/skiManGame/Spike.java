package com.me.skiManGame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Spike {
	Rectangle collisionArea;
	TextureRegion spikeImage;
	float rotation = 0;
	
	public Spike(){
		
	}
	
	public Spike(TextureAtlas atlas, float x, float y){
		collisionArea = new Rectangle(x, y, 20, 20);
		spikeImage = atlas.findRegion("icicle");
	}
	
	public Spike(TextureAtlas atlas, float x, float y, float rot){
		collisionArea = new Rectangle(x, y, 20, 20);
		spikeImage = atlas.findRegion("icicle");
		rotation = rot;
	}
	
	public void render(SpriteBatch batch){
		batch.draw(spikeImage, collisionArea.x, collisionArea.y, collisionArea.width / 2, collisionArea.height / 2, collisionArea.width, collisionArea.height, 1, 1, rotation);
	}
	
	public void setPosition(float x, float y){
		collisionArea.x = x;
		collisionArea.y = y;
	}
	
	public Vector2 getPosition(){
		return new Vector2(collisionArea.x, collisionArea.y);
	}
}
