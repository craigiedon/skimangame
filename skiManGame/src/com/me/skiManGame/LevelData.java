package com.me.skiManGame;

import java.util.ArrayList;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

public class LevelData {
	public Vector2 playerStart;
	public Vector2 goal;
	public ArrayList<Polygon> polygons;
	public ArrayList<SpikeInfo> spikesInfo;
	public ArrayList<DetectorInfo> detectorsInfo;
	
	public LevelData(){
		playerStart = new Vector2(0,0);
		polygons = new ArrayList<Polygon>();
		spikesInfo = new ArrayList<SpikeInfo>();
		detectorsInfo = new ArrayList<DetectorInfo>();
	}
}
