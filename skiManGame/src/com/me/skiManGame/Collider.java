package com.me.skiManGame;

import java.util.ArrayList;
import java.util.Collections;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Collider {
	
	Vector2 project(Vector2 vec, Vector2 axis){
		float dotProd = vec.dot(axis);
		Vector2 normalizedAxis = axis.cpy().nor();
		float magnitude = dotProd / axis.len();
		return new Vector2(magnitude * normalizedAxis.x, magnitude * normalizedAxis.y);
	}
	
	static Vector2 leftNormal(Vector2 vec){
		return new Vector2(-vec.y, vec.x);
	}
	
	public static CollisionInfo playerRectCollisionSAT(Rectangle rectangle, Polygon polygon){
		Vector2 oldCoords = new Vector2();
		rectangle.getPosition(oldCoords);
		ArrayList<LineSegment> polyLines = new ArrayList<LineSegment>();
		ArrayList<Vector2> axisNormals = new ArrayList<Vector2>();
		float[] polyPoints = polygon.getTransformedVertices();

		
		//Calculate each of the normals by making a vector from the first point to the second point, then getting the vector perpendicular to this (on the left hand side of it)
		for(int i = 0; i < polyPoints.length - 3; i+=2){
			Vector2 firstPoint = new Vector2(polyPoints[i], polyPoints[i + 1]);
			Vector2 secondPoint = new Vector2(polyPoints[i + 2], polyPoints[i + 3]);
			polyLines.add(new LineSegment(firstPoint.x, firstPoint.y, secondPoint.x, secondPoint.y));
			axisNormals.add(leftNormal(new Vector2(secondPoint.x - firstPoint.x, secondPoint.y - firstPoint.y)).nor());
		}
		
		//Final line segment needs special consideration as it is made by joining the last point back up to the first
		int numPoints = polyPoints.length;
		Vector2 penultimatePoint = new Vector2(polyPoints[numPoints - 2], polyPoints[numPoints - 1]);
		Vector2 finalPoint = new Vector2(polyPoints[0], polyPoints[1]);
		
		polyLines.add(new LineSegment(penultimatePoint.x, penultimatePoint.y, finalPoint.x, finalPoint.y));
		axisNormals.add(leftNormal(new Vector2(finalPoint.x - penultimatePoint.x, finalPoint.y - penultimatePoint.y)).nor());
	
		//Rectangle normals
		axisNormals.add(leftNormal(new Vector2(rectangle.width, 0)).nor());
		axisNormals.add(leftNormal(new Vector2(0, rectangle.height)).nor());
		
		ArrayList<Float> overlaps = new ArrayList<Float>();
		
		for(int normalIndex = 0; normalIndex < axisNormals.size(); normalIndex++){
			Vector2 axisNormal = axisNormals.get(normalIndex);
			//project rectangle points
			ArrayList<Float> rectangleDPs = new ArrayList<Float>();
			
			rectangleDPs.add(axisNormal.dot(new Vector2(rectangle.x, rectangle.y)));
			rectangleDPs.add(axisNormal.dot(new Vector2(rectangle.x, rectangle.y + rectangle.height)));
			rectangleDPs.add(axisNormal.dot(new Vector2(rectangle.x + rectangle.width, rectangle.y + rectangle.height)));
			rectangleDPs.add(axisNormal.dot(new Vector2(rectangle.x + rectangle.width, rectangle.y)));

			float rectMinDP = Collections.min(rectangleDPs);
			float rectMaxDP = Collections.max(rectangleDPs);
					
			
			//project the polygon onto it
			float polyMinDP = axisNormal.dot(new Vector2(polyPoints[0], polyPoints[1]));
			float polyMaxDP = axisNormal.dot(new Vector2(polyPoints[0], polyPoints[1]));
			for(int i = 0; i < polyPoints.length - 1; i+=2){
				float currentDotProduct = axisNormal.dot(new Vector2(polyPoints[i], polyPoints[i+1]));
				if(currentDotProduct < polyMinDP){
					polyMinDP = currentDotProduct;
				}
				
				if(currentDotProduct > polyMaxDP){
					polyMaxDP = currentDotProduct;
				}
			}
			
			//Find if there is any overlap by inspection of dot products
			float overlap;
			if(normalIndex < axisNormals.size() - 2){
				overlap = polyMaxDP - rectMinDP;
				if(overlap <= 0){
					return new CollisionInfo(CollisionType.NONE, null);
				}
			}
			else{
				if(rectMinDP < polyMinDP){
					overlap = polyMinDP - rectMaxDP;
					if(overlap >= 0){
						return new CollisionInfo(CollisionType.NONE, null);
					}
				}
				else{
					overlap = polyMaxDP - rectMinDP;
					if(overlap <= 0){
						return new CollisionInfo(CollisionType.NONE, null);
					}
				}
			}
			
			//Can stop immediately if ANY side does not overlap (Separating axis theorem)
			overlaps.add(overlap);
		}
		
		//Separate along axis of smallest overlap
		int minOverlapIndex = 0;
		float minOverlap = overlaps.get(0);
		for(int overlapIndex = 0; overlapIndex < overlaps.size(); overlapIndex++){
			if(Math.abs(overlaps.get(overlapIndex)) < Math.abs(minOverlap)){
				minOverlapIndex = overlapIndex;
				minOverlap = overlaps.get(overlapIndex);
			}
		}
		
		rectangle.x += minOverlap * axisNormals.get(minOverlapIndex).x;
		rectangle.y += minOverlap * axisNormals.get(minOverlapIndex).y;
		
		if(rectangle.y > oldCoords.y){
			return new CollisionInfo(CollisionType.GROUND, axisNormals.get(minOverlapIndex));
		}
		else{
			return new CollisionInfo(CollisionType.NORMAL, axisNormals.get(minOverlapIndex));
		}
		
	}

	public static CollisionInfo playerRectCollision(Rectangle rectangle, Polygon polygon){
		Vector2 oldCoords = new Vector2();
		rectangle.getPosition(oldCoords);
		ArrayList<LineSegment> polyLines = new ArrayList<LineSegment>();
		ArrayList<Vector2> axisNormals = new ArrayList<Vector2>();
		float[] polyPoints = polygon.getTransformedVertices();

		
		//Calculate each of the normals by making a vector from the first point to the second point, then getting the vector perpendicular to this (on the left hand side of it)
		for(int i = 0; i < polyPoints.length - 3; i+=2){
			Vector2 firstPoint = new Vector2(polyPoints[i], polyPoints[i + 1]);
			Vector2 secondPoint = new Vector2(polyPoints[i + 2], polyPoints[i + 3]);
			polyLines.add(new LineSegment(firstPoint.x, firstPoint.y, secondPoint.x, secondPoint.y));
			axisNormals.add(leftNormal(new Vector2(secondPoint.x - firstPoint.x, secondPoint.y - firstPoint.y)).nor());
		}
		
		//Final line segment needs special consideration as it is made by joining the last point back up to the first
		int numPoints = polyPoints.length;
		Vector2 penultimatePoint = new Vector2(polyPoints[numPoints - 2], polyPoints[numPoints - 1]);
		Vector2 finalPoint = new Vector2(polyPoints[0], polyPoints[1]);
		
		polyLines.add(new LineSegment(penultimatePoint.x, penultimatePoint.y, finalPoint.x, finalPoint.y));
		axisNormals.add(leftNormal(new Vector2(finalPoint.x - penultimatePoint.x, finalPoint.y - penultimatePoint.y)).nor());
	
		//Rectangle normals
		axisNormals.add(leftNormal(new Vector2(rectangle.width, 0)).nor());
		axisNormals.add(leftNormal(new Vector2(0, rectangle.height)).nor());
		
		ArrayList<Float> overlaps = new ArrayList<Float>();
		
		for(int normalIndex = 0; normalIndex < axisNormals.size(); normalIndex++){
			Vector2 axisNormal = axisNormals.get(normalIndex);
			//project rectangle points
			ArrayList<Float> rectangleDPs = new ArrayList<Float>();
			
			rectangleDPs.add(axisNormal.dot(new Vector2(rectangle.x, rectangle.y)));
			rectangleDPs.add(axisNormal.dot(new Vector2(rectangle.x, rectangle.y + rectangle.height)));
			rectangleDPs.add(axisNormal.dot(new Vector2(rectangle.x + rectangle.width, rectangle.y + rectangle.height)));
			rectangleDPs.add(axisNormal.dot(new Vector2(rectangle.x + rectangle.width, rectangle.y)));

			float rectMinDP = Collections.min(rectangleDPs);
			float rectMaxDP = Collections.max(rectangleDPs);
					
			
			//project the polygon onto it
			float polyMinDP = axisNormal.dot(new Vector2(polyPoints[0], polyPoints[1]));
			float polyMaxDP = axisNormal.dot(new Vector2(polyPoints[0], polyPoints[1]));
			for(int i = 0; i < polyPoints.length - 1; i+=2){
				float currentDotProduct = axisNormal.dot(new Vector2(polyPoints[i], polyPoints[i+1]));
				if(currentDotProduct < polyMinDP){
					polyMinDP = currentDotProduct;
				}
				
				if(currentDotProduct > polyMaxDP){
					polyMaxDP = currentDotProduct;
				}
			}
			
			//Find if there is any overlap by inspection of dot products
			float overlap;
			if(normalIndex < axisNormals.size() - 2){
				overlap = polyMaxDP - rectMinDP;
				if(overlap <= 0){
					return new CollisionInfo(CollisionType.NONE, null);
				}
			}
			else{
				if(rectMinDP < polyMinDP){
					overlap = polyMinDP - rectMaxDP;
					if(overlap >= 0){
						return new CollisionInfo(CollisionType.NONE, null);
					}
				}
				else{
					overlap = polyMaxDP - rectMinDP;
					if(overlap <= 0){
						return new CollisionInfo(CollisionType.NONE, null);
					}
				}
			}
			
			//Can stop immediately if ANY side does not overlap (Separating axis theorem)
			overlaps.add(overlap);
		}
		
		//Separate along axis of smallest overlap
		int minOverlapIndex = 0;
		float minOverlap = overlaps.get(0);
		for(int overlapIndex = 0; overlapIndex < overlaps.size(); overlapIndex++){
			if(Math.abs(overlaps.get(overlapIndex)) < Math.abs(minOverlap)){
				minOverlapIndex = overlapIndex;
				minOverlap = overlaps.get(overlapIndex);
			}
		}
		
		
		if(minOverlapIndex < axisNormals.size() - 2 &&
		   axisNormals.get(minOverlapIndex).y > 0.25 &&
		   axisNormals.get(minOverlapIndex).x != 0){
			LineSegment collisionLine = polyLines.get(minOverlapIndex);
			float[] yOffsets = new float[4];
			yOffsets[0] = calcYOffset(rectangle.x, rectangle.y, collisionLine);
			yOffsets[1] = calcYOffset(rectangle.x + rectangle.width, rectangle.y, collisionLine);
			yOffsets[2] = calcYOffset(rectangle.x + rectangle.width, rectangle.y + rectangle.height, collisionLine);
			yOffsets[3] = calcYOffset(rectangle.x, rectangle.y + rectangle.height, collisionLine);
			
			float maxYOffset = 0;
			for(float yOffset : yOffsets){
				if(yOffset > maxYOffset){
					maxYOffset = yOffset;
				}
			}
			
			//We are looking for the biggest positive offset if it is an upward facing slope;
			float highestPoint = Math.max(collisionLine.startY, collisionLine.endY);
			rectangle.y += maxYOffset;
			if(rectangle.y > highestPoint){
				rectangle.y = highestPoint;
				playerRectCollision(rectangle, polygon);
			}
		}
		else{
			rectangle.x += minOverlap * axisNormals.get(minOverlapIndex).x;
			rectangle.y += minOverlap * axisNormals.get(minOverlapIndex).y;
		}
		
		if(rectangle.y > oldCoords.y){
			return new CollisionInfo(CollisionType.GROUND, axisNormals.get(minOverlapIndex));
		}
		else{
			return new CollisionInfo(CollisionType.NORMAL, axisNormals.get(minOverlapIndex));
		}
	}
	
	public static float calcYOffset(float xPos, float yPos, LineSegment line){
		float vectorRatio = (xPos - line.startX) / line.getVector().x;
		float newY = line.startY + vectorRatio * line.getVector().y;
		return newY - yPos;
	}
}
