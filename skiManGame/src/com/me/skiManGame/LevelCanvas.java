package com.me.skiManGame;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Json;

public class LevelCanvas extends InputAdapter{
	Vector2 currentShapeStart;
	Vector2 currentShapeEnd;
	
	ArrayList<Polygon> polygons;
	ArrayList<Spike> spikes;
	Spike selectedSpike = null;
	
	ArrayList<Detector> detectors;
	Detector newDetector = null;
	Detector selectedDetector = null;
	
	Vector2 currentMousePos;
	Tool currentTool;
	public static final int NO_SELECTION = -1;
	
	OrthographicCamera camera;
	TextureAtlas atlas;
	
	Vector2 playerStart = new Vector2(10,10);
	Goal goal;
	TextureRegion goalImage;
	
	int selectedPolygonID = NO_SELECTION;
	Vector2 originalMousePosition = null;
	Vector2 oldSelectedPosition = null;
	
	public LevelCanvas(OrthographicCamera cam){
		polygons = new ArrayList<Polygon>();
		spikes = new ArrayList<Spike>();
		detectors = new ArrayList<Detector>();
		currentMousePos = new Vector2(0,0);
		currentTool = Tool.CREATE_RECTANGLE;
		camera = cam;
		atlas = new TextureAtlas(Gdx.files.internal("placeHolderGraphics.atlas"));
		goal = new Goal(new Vector2(200,200), atlas);
	}
	
	public Polygon getSelectedShape(){
		if(selectedPolygonID != NO_SELECTION && polygons.size() > 0){
			return polygons.get(selectedPolygonID);
		}
		else{
			return null;
		}
	}
	
	public boolean isManipulationTool(Tool tool){
		return (tool == Tool.ROTATE || tool == Tool.TRANSLATE || tool == Tool.SCALE);
	}
	
	public boolean isShapeTool(Tool tool){
		return (tool == Tool.CREATE_RECTANGLE || tool == Tool.CREATE_TRIANGLE);
	}
	
	public boolean touchDown(int screenX, int screenY, int pointer, int button){
		Vector3 worldCoords = new Vector3(screenX, screenY, 0);
		camera.unproject(worldCoords);
		
		selectedPolygonID = NO_SELECTION;
		selectedSpike = null;
		selectedDetector = null;

		if(isManipulationTool(currentTool)){
			boolean isSomethingSelected = false;
			for(int i = 0; i < polygons.size() ;i++){
				if(polygons.get(i).contains(worldCoords.x, worldCoords.y)){
					selectedPolygonID = i;
					originalMousePosition = new Vector2(worldCoords.x, worldCoords.y);
					oldSelectedPosition = new Vector2(polygons.get(i).getX(), polygons.get(i).getY());
					isSomethingSelected = true;
					break;
				}
			}
			if(!isSomethingSelected){
				for(Spike spike : spikes){
					if(spike.collisionArea.contains(worldCoords.x, worldCoords.y)){
						selectedSpike = spike;
						originalMousePosition = new Vector2(worldCoords.x, worldCoords.y);
						oldSelectedPosition = new Vector2(spike.collisionArea.x, spike.collisionArea.y);
						isSomethingSelected = true;
						
						if(currentTool == Tool.ROTATE){
							spike.rotation = (spike.rotation + 90) % 360;
						}
					}
				}
			}
			if(!isSomethingSelected){
				for(Detector detector : detectors){
					if(detector.collisionArea.contains(worldCoords.x, worldCoords.y)){
						selectedDetector = detector;
						originalMousePosition = new Vector2(worldCoords.x, worldCoords.y);
						oldSelectedPosition = new Vector2(detector.collisionArea.x, detector.collisionArea.y);
						isSomethingSelected = true;
					}
				}
			}
		}
		else if(isShapeTool(currentTool)){
			currentShapeStart = new Vector2(worldCoords.x, worldCoords.y);
			currentShapeEnd = new Vector2(worldCoords.x, worldCoords.y);
		}
		else if(currentTool == Tool.CREATE_DETECTOR){
			newDetector = new Detector(worldCoords.x, worldCoords.y, atlas);
		}
		else if(currentTool == Tool.DELETE){
			for(int i = 0; i < polygons.size() ;i++){
				if(polygons.get(i).contains(worldCoords.x, worldCoords.y)){
					polygons.remove(i);
					return true;
				}
			}
			for(int i = 0; i < spikes.size(); i++){
				if(spikes.get(i).collisionArea.contains(worldCoords.x, worldCoords.y)){
					spikes.remove(i);
					return true;
				}
			}
			for(int i = 0; i < detectors.size(); i++){
				if(detectors.get(i).collisionArea.contains(worldCoords.x, worldCoords.y)){
					detectors.remove(i);
					return true;
				}
			}
		}
		
		return true;
	}
	
	public boolean touchUp(int screenX, int screenY, int pointer, int button){
		Vector3 worldCoords = new Vector3(screenX, screenY, 0);
		camera.unproject(worldCoords);
		
		if(isManipulationTool(currentTool)){
		}
		else if(currentTool == Tool.SET_START){
			playerStart.set(worldCoords.x, worldCoords.y);
		}
		else if(currentTool == Tool.SET_GOAL){
			goal.setPosition(worldCoords.x, worldCoords.y);
		}
		else if(currentTool == Tool.CREATE_SPIKE){
			spikes.add(new Spike(atlas, worldCoords.x, worldCoords.y));
		}
		else if(isShapeTool(currentTool)){
			currentShapeEnd.x = worldCoords.x;
			currentShapeEnd.y = worldCoords.y;
			
			polygons.add(createPolyShape(currentShapeStart, currentShapeEnd, currentTool));
			currentShapeStart = null;
			currentShapeEnd = null;
		}
		else if(currentTool == Tool.CREATE_DETECTOR && newDetector != null){
			if(newDetector.collisionArea.width < 0){
				newDetector.collisionArea.x += newDetector.collisionArea.width;
				newDetector.collisionArea.width *= -1;
			}
			if(newDetector.collisionArea.height < 0){
				newDetector.collisionArea.y += newDetector.collisionArea.height;
				newDetector.collisionArea.height *= -1;
			}
			detectors.add(newDetector);
			newDetector = null;
		}
		return true;
	}
	
	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer){
		Vector3 worldCoords = new Vector3(screenX, screenY, 0);
		camera.unproject(worldCoords);
		if(currentTool == Tool.TRANSLATE){
			if(selectedDetector != null){
				selectedDetector.setPosition(oldSelectedPosition.x + (worldCoords.x - originalMousePosition.x), 
											 oldSelectedPosition.y + (worldCoords.y - originalMousePosition.y));
			}
			else if(selectedPolygonID != NO_SELECTION){
				polygons.get(selectedPolygonID).setPosition(oldSelectedPosition.x + (worldCoords.x - originalMousePosition.x),
															oldSelectedPosition.y + (worldCoords.y - originalMousePosition.y));
			}
			else if(selectedSpike != null){
				selectedSpike.setPosition(oldSelectedPosition.x + (worldCoords.x - originalMousePosition.x),
										  oldSelectedPosition.y + (worldCoords.y - originalMousePosition.y));
			}
		}
		else if(currentTool == Tool.ROTATE && selectedPolygonID != NO_SELECTION){
			Polygon currentPoly = polygons.get(selectedPolygonID);
			Vector2 centreToOriginal = new Vector2(originalMousePosition.x - getCentre(currentPoly).x, originalMousePosition.y - getCentre(currentPoly).y);
			Vector2 centreToNew = new Vector2(worldCoords.x - getCentre(currentPoly).x, worldCoords.y - getCentre(currentPoly).y);
			float amountToRotate = centreToNew.angle() - centreToOriginal.angle();
			currentPoly.rotate(amountToRotate);
			originalMousePosition.x = worldCoords.x;
			originalMousePosition.y = worldCoords.y;
		}
		else if(currentTool == Tool.SCALE){
			if(selectedPolygonID != NO_SELECTION){
				Polygon currentPoly = polygons.get(selectedPolygonID);
				currentPoly.setScale(currentPoly.getScaleX() + (worldCoords.x - originalMousePosition.x) / 100, currentPoly.getScaleY() + (worldCoords.y - originalMousePosition.y) / 100);
				originalMousePosition.x = worldCoords.x;
				originalMousePosition.y = worldCoords.y;
			}
		}
		else if(isShapeTool(currentTool)){
			currentShapeEnd.x = worldCoords.x;
			currentShapeEnd.y = worldCoords.y;
		}
		else if(currentTool == Tool.SET_START){
			playerStart.set(worldCoords.x, worldCoords.y);
		}
		else if(currentTool == Tool.SET_GOAL){
			goal.setPosition(worldCoords.x, worldCoords.y);
		}
		else if(currentTool == Tool.CREATE_DETECTOR && newDetector != null){
			newDetector.collisionArea.width = worldCoords.x - newDetector.getPosition().x;
			newDetector.collisionArea.height = worldCoords.y - newDetector.getPosition().y;
		}
			return true;
	}
	
	public boolean keyDown(int keycode){
		if(selectedPolygonID != NO_SELECTION){
			Polygon poly = polygons.get(selectedPolygonID);
			if(keycode == Keys.LEFT){
				poly.setPosition(poly.getX() - 1, poly.getY());
			}
			if(keycode == Keys.RIGHT){
				poly.setPosition(poly.getX() + 1,  poly.getY());
			}
			if(keycode == Keys.UP){
				poly.setPosition(poly.getX(), poly.getY() + 1);
			}
			if(keycode == Keys.DOWN){
				poly.setPosition(poly.getX(), poly.getY() - 1);
			}
		}
		return true;
	}
	
	public void render(skiManGame game){
		ShapeRenderer shapeRenderer = game.shapeRenderer;
		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.setColor(Color.BLACK);
		
		for(int i = 0; i < polygons.size(); i++){
			Polygon poly = polygons.get(i);
			if(i == selectedPolygonID){
				shapeRenderer.setColor(Color.RED);
			}
			else{
				shapeRenderer.setColor(Color.BLACK);
			}
			shapeRenderer.polygon(poly.getTransformedVertices());
			shapeRenderer.circle(getCentre(poly).x, getCentre(poly).y, 2);
		}
		
		if(currentShapeStart != null && currentShapeEnd != null){
			shapeRenderer.polygon(createPolyShape(currentShapeStart, currentShapeEnd, currentTool).getTransformedVertices());
		}
		shapeRenderer.end();
		
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(Color.BLUE);
		shapeRenderer.circle(playerStart.x, playerStart.y, 5);
		shapeRenderer.end();
		
		game.batch.begin();
		for(Spike spike : spikes){
			spike.render(game.batch);
		}
		for(Detector detector : detectors){
			detector.render(game.batch);
		}
		if(newDetector != null){
			newDetector.render(game.batch);
		}
		goal.render(game.batch);
		game.batch.end();
	}
	
	public Polygon createPolyShape(Vector2 start, Vector2 end, Tool tool){
		//Remember, always create polygon points in a clockwise direction
		Polygon createdPolygon = null;
		switch(tool){
		case CREATE_RECTANGLE:
			Vector2 lowerLeft = new Vector2(Math.min(start.x,  end.x), Math.min(start.y, end.y));
			Vector2 upperRight = new Vector2(Math.max(start.x, end.x), Math.max(start.y, end.y));
			createdPolygon = new Polygon(new float[]{lowerLeft.x, lowerLeft.y,
					lowerLeft.x, upperRight.y,
					upperRight.x, upperRight.y,
					upperRight.x, lowerLeft.y});
			break;
		case CREATE_TRIANGLE:
			if(end.x > start.x && end.y > start.y){
				createdPolygon = new Polygon(new float[]{start.x, start.y,
														 end.x, end.y,
														 end.x, start.y});
			}
			else if(end.x > start.x && end.y <= start.y){
				createdPolygon = new Polygon(new float[]{start.x, start.y,
														 end.x, end.y,
														 start.x, end.y});
			}
			else if(end.x <= start.x && end.y > start.y){
				createdPolygon = new Polygon(new float[]{end.x, end.y,
														 start.x, start.y,
														 end.x, start.y});
			}
			else if(end.x <= start.x && end.y <= start.y){
				createdPolygon = new Polygon(new float[]{start.x, start.y,
														 start.x, end.y,
														 end.x, end.y});
			}
			break;
		default:
			break;
		}

		Vector2 centrePoint = getCentre(createdPolygon);
		float[] vertices = createdPolygon.getVertices();
		for(int i = 0; i < createdPolygon.getVertices().length - 1; i+=2){
			vertices[i] -= centrePoint.x;
			vertices[i + 1] -= centrePoint.y;
		}
		
		createdPolygon.setVertices(vertices);
		createdPolygon.setPosition(centrePoint.x, centrePoint.y);
		return createdPolygon;
	}
	
	public void clear(){
		polygons.clear();
		spikes.clear();
		detectors.clear();
		playerStart.set(10,10);
	}
	
	public Vector2 getCentre(Polygon poly){
		float xSum = 0;
		float ySum = 0;
		float[] polyVerts = poly.getTransformedVertices();
		for(int i = 0; i < poly.getTransformedVertices().length - 1; i+=2){
			xSum += polyVerts[i];
			ySum += polyVerts[i + 1];
		}
		
		return new Vector2(xSum / (polyVerts.length * 0.5f), ySum / (polyVerts.length * 0.5f));
	}
	
	public void saveLevel(String levelName){
		LevelData levelData = new LevelData();
		levelData.polygons = polygons;
		levelData.playerStart = playerStart;
		levelData.goal = goal.getPosition();
		for(Spike spike : spikes){
			levelData.spikesInfo.add(new SpikeInfo(spike.getPosition(), spike.rotation));
		}
		
		ArrayList<DetectorInfo> detectorsInfo = new ArrayList<DetectorInfo>();
		for(Detector detector: detectors){
			levelData.detectorsInfo.add(new DetectorInfo(detector.getPosition(), detector.getDimensions()));
		}
		
		Json json = new Json();
		FileHandle saveFile = Gdx.files.local("levels/" + levelName);
		saveFile.writeString(json.toJson(levelData, levelData.getClass()), false);		
	}
	
	public void loadLevel(String levelName){
		Json json = new Json();
		LevelData levelData = new LevelData();
		levelData = json.fromJson(levelData.getClass(), Gdx.files.local("levels/" + levelName));
		polygons = levelData.polygons;
		playerStart = levelData.playerStart;
		spikes.clear();
		for(SpikeInfo spikeInfo: levelData.spikesInfo){
			spikes.add(new Spike(atlas, spikeInfo.location.x, spikeInfo.location.y, spikeInfo.rotation));
		}
		detectors.clear();
		for(DetectorInfo detectorInfo : levelData.detectorsInfo){
			detectors.add(new Detector(detectorInfo.position, detectorInfo.dimensions, atlas));
		}
		goal.setPosition(levelData.goal.x, levelData.goal.y);
	}
}
