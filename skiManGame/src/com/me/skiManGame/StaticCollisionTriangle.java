package com.me.skiManGame;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

public class StaticCollisionTriangle {
	
	Vector2 p1;
	Vector2 p2;
	Vector2 p3;
	float width;
	float height;
	
	Vector2 hypotenuse;
	
	public StaticCollisionTriangle(Vector2 v1, Vector2 v2, Vector2 v3){
		p1 = v1;
		p2 = v2;
		p3 = v3;
		hypotenuse = new Vector2(v3.x - v2.x, v3.y - v2.y);
		width = Math.abs(v3.x - v1.x);
		height = Math.abs(v2.y - v1.y);
	}
	
	public void update(){
		
	}
	
	public void render(skiManGame game){
		game.batch.end();
		game.shapeRenderer.begin(ShapeType.Filled);
		game.shapeRenderer.setColor(Color.RED);
		game.shapeRenderer.triangle(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
		game.shapeRenderer.end();
		game.batch.begin();
		
	}
}
